<?php
   require_once('animal.php');
   require_once('frog.php');
   require_once('ape.php');
   
   $sheep = new animal("shaun");

   echo "Nama hewan : $sheep->name <br>"; // "shaun"
   echo "Jumlah kaki : $sheep->legs <br>"; // 2
   echo "Termasuk kedalam hewan berdarah dingin : $sheep->cold_blooded <br><br>";// false

   $sungokong = new ape("kera sakti");
   echo "Nama hewan : $sungokong->name <br>"; 
   echo "Jumlah kaki : $sungokong->legs <br>"; // 2
   echo "Termasuk kedalam hewan berdarah dingin : $sungokong->cold_blooded <br>";// false
   echo $sungokong->yell() ; // "Auooo"
   echo "<br><br>";

   $kodok = new frog("buduk");
   echo "Nama hewan : $kodok->name <br>"; 
   echo "Jumlah kaki : $kodok->legs <br>"; // 2
   echo "Termasuk kedalam hewan berdarah dingin : $kodok->cold_blooded <br>";// false
   echo $kodok->jump(); // "hop hop"
   echo "<br><br>";

?>